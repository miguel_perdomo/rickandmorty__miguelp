import { Component, OnInit } from '@angular/core';
import { LocationsService } from 'src/app/shared/services/locations.service';

@Component({
  selector: 'app-locations-page',
  templateUrl: './locations-page.component.html',
  styleUrls: ['./locations-page.component.scss']
})
export class LocationsPageComponent implements OnInit {

  locations:any;

  constructor(private apiLocations:LocationsService) { }

  ngOnInit(): void {
    this.apiLocations.getLocations().subscribe((data:any) =>
    {
      this.locations = data.results;
    })
  }

}
