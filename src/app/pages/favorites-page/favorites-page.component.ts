import { Component, OnInit } from '@angular/core';
import { FavoritesLocalService } from 'src/app/shared/services/local/favorites-local.service';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.scss']
})
export class FavoritesPageComponent implements OnInit {

  favList = [];


  constructor(private favServiceList: FavoritesLocalService) { }

  ngOnInit(): void {
    this.favList = this.favServiceList.getFavorites();
  }

}
