import { Component, OnInit } from '@angular/core';
import { CharactersService } from 'src/app/shared/services/characters.service';

@Component({
  selector: 'app-characters-page',
  templateUrl: './characters-page.component.html',
  styleUrls: ['./characters-page.component.scss']
})
export class CharactersPageComponent implements OnInit {

  characters: any;
  actualPage = 1;
  find: string = "";

  constructor(private apiCharacters:CharactersService) { }

  ngOnInit(): void {
    // this.apiCharacters.getCharacters().subscribe((data: any) => {
    //   this.characters = data.results;
    // })
    this.getElement('', 1);

  }

  getElement( allData: any, actualPage: any){
    this.actualPage = actualPage;
    this.apiCharacters.getCharacters(allData.toLowerCase() , actualPage).subscribe((data: any) => {
      this.characters = data.results;
    })
  }
  
}
