import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priorityName'
})
export class PriorityNamePipe implements PipeTransform {

  transform(value: string, arg1: string, arg2:any ): string {
    if(value.includes(arg1)){
      return arg2 + value + arg2
    }
    return value;
  }

}
