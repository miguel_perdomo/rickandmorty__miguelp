import { Component, Input, OnInit } from '@angular/core';
import { FavoritesLocalService } from '../../services/local/favorites-local.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() list: any;

  constructor(private favService: FavoritesLocalService) { }

  ngOnInit(): void {
    
  }

  favoriteItems(itemFav:any){
    this.favService.addFavorite(itemFav)
    console.log(this.favService.getFavorites())
  }


}
